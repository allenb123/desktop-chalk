#!/bin/bash

# Chalk: A desktop environment based on openbox and polybar.

DIR=$(dirname "$0")

xrdb -merge $DIR/urxvt.xres
xrdb -merge $DIR/colors.xres

hsetroot -extend $DIR/wallpaper.jpg
(
	sleep 0.1
	polybar -c $DIR/polybar/polybar.ini main &
) &
picom -e 1 -i 1 -m 1 -c -C -r 4 -t -3 -l -3 -o 0.65 -D 5 -I 0.1 -O 0.1 &

openbox --config-file "$DIR/openbox/rc.xml"
