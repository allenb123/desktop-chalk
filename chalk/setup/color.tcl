#!/usr/bin/tclsh

array set colors [list \
	"nord0" "#2e3440" \
	"nord1" "#3b4252" \
	"nord2" "#434c5e" \
	"nord3" "#4c566a" \
	"nord4" "#d8dee9" \
	"nord5" "#e5e9f0" \
	"nord6" "#eceff4" \
	"nord7" "#8fbcbb" \
	"nord8" "#88c0d0" \
	"nord9" "#81a1c1" \
	"nord10" "5e81ac" \
	"nord11" "#bf616a" \
	"nord12" "#d08770" \
	"nord13" "#ebcb8b" \
	"nord14" "#a3be8c" \
	"nord15" "#b48ead" ]

set fp [open "../colors.sh" "w"]
foreach {color hex} [array get colors] {
	puts $fp [format {%s="%s"} "$color" "$hex"]
}
close $fp

set fp [open "../colors.xres" "w"]

set darkcolors [split " #bf616a #a3be8c #d08770 #5e81ac #b48ead #88c0d0" " "]
set lightcolors [split " #bf616a #a3be8c #ebcb8b #81a1c1 #b48ead #88c0d0" " "]
set white #eceff4
set gray #4c566a
set black #2e3440

for {set i 1} {$i <= 6} {incr i} {
	set color [lindex $darkcolors $i]
	puts $fp "URxvt.color$i: $color"

	set color [lindex $lightcolors $i]
	puts $fp "URxvt.color[expr $i+8]: $color"
}

puts $fp "URxvt.color7: $white"
puts $fp "URxvt.color15: $white"
puts $fp "URxvt.color0: $black"
puts $fp "URxvt.color8: $gray"

close $fp
