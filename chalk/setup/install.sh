#!/bin/bash
DIR=`pwd`

# check for the correct directory
if [[ "$DIR" == *chalk ]] ; then
	true
else
	echo "invalid directory"
	exit 1
fi

# install .desktop file
exec_path="$DIR/chalk.sh" 
result="$(cat $DIR/chalk.desktop)"
result="${result//EXEC_PATH/$exec_path}"
echo "$result" > /usr/share/xsessions/chalk.desktop

# install openbox menu
cp "$DIR/openbox/menu.xml" /etc/xdg/openbox/chalk.xml

# install openbox theme
mv /usr/share/themes/Chalk `mktemp -d`
cp -r "$DIR/../theme" /usr/share/themes/Chalk

#mkdir ~/.config
#mkdir ~/.config/openbox
#cp "$DIR/openbox/rc.xml" ~/.config/openbox/rc.xml