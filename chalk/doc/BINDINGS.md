# Keybindings

See `openbox/rc.xml` for a full list.

Themes:
 * Super = window management
 * Ctrl+Alt = workspace management
 * Alt+Shift = Apps